﻿namespace Twitterizer.RateLimitStatus
{
	using System;
    using Newtonsoft.Json;
    using System.Runtime.Serialization;    
    using Twitterizer.Core;
    
    /// <summary>
    /// The Application of rate limit status.
    /// </summary>
    /// 
#if !SILVERLIGHT
    [Serializable]
#endif    
   [JsonObject(MemberSerialization = MemberSerialization.OptIn)]
    public class RateLimitApplication : TwitterObject
    {
        #region API Properties
        
        /// <summary>
        /// Gets or sets the settings.
        /// </summary>
        /// <value>The settings rate-limit.</value>
        [DataMember, JsonProperty(PropertyName = "/application/rate_limit_status")]
        public TwitterRateLimitResults RateLimitStatus { get; set; }

        #endregion
}
}