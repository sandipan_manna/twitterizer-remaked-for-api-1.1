﻿namespace Twitterizer.RateLimitStatus
	
{
	using System;
    using Newtonsoft.Json;
    using System.Runtime.Serialization;    
    using Twitterizer.Core;
    
    /// <summary>
    /// The Favorites of rate limit status.
    /// </summary>
    /// 
#if !SILVERLIGHT
    [Serializable]
#endif    
   [JsonObject(MemberSerialization = MemberSerialization.OptIn)]
    public class RateLimitFavorites : TwitterObject
    {
        #region API Properties
        
        /// <summary>
        /// Gets or sets the list.
        /// </summary>
        [DataMember, JsonProperty(PropertyName = "/favorites/list")]
        public TwitterRateLimitResults List { get; set; }
        
        #endregion
}
}